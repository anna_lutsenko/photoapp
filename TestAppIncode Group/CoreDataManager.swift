//
//  CoreDataManager.swift
//  TestAppIncode Group
//
//  Created by Anna on 07.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager : DataProviderProtocol {
    
    static let instance = CoreDataManager()
    
    private init() {}
    
    func getPhotos(succeess: @escaping ([PhotoModel]) -> (), failure: @escaping (Error) -> ()) {
        let moc = managedObjectContext
        let photosFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "PhotoEntity")
        
        let sortDescriptor = NSSortDescriptor(key: "photoEntityId", ascending: true)
        photosFetch.sortDescriptors = [sortDescriptor]
        
        do {
            let objs = try moc.fetch(photosFetch) as! [PhotoEntity]
            succeess(objs)
        } catch {
            failure(error)
        }
    }
    
    func saveToSorage(photos: [PhotoModel]) {
        var photosArr = [NSManagedObject]()
        for photo in photos {
            let managedObject = PhotoEntity()
            managedObject.photoEntityId = Int16(photo.id)
            managedObject.photoEntityTitle = photo.title
            managedObject.photoEntityURL = photo.url
            
            for comment in photo.comments {
                let commentManagedObj = CommentEntity()
                commentManagedObj.commentEntityId = Int16(comment.id)
                commentManagedObj.commentEntityEmail = comment.email
                commentManagedObj.commentEntityName = comment.name
                commentManagedObj.commentEntityBody = comment.body
                managedObject.addToComment(commentManagedObj)
            }
            
            photosArr.append(managedObject)
        }
        saveContext()
    }
    
    func clearAllEnteties() {
        let photosFR = NSFetchRequest<NSFetchRequestResult>(entityName: "PhotoEntity")
        let commentFR = NSFetchRequest<NSFetchRequestResult>(entityName: "CommentEntity")
        
        let deletePhotosRequest = NSBatchDeleteRequest(fetchRequest: photosFR)
        let deleteCommentsRequest = NSBatchDeleteRequest(fetchRequest: commentFR)
        
        do {
            try persistentStoreCoordinator.execute(deletePhotosRequest, with: managedObjectContext)
            try persistentStoreCoordinator.execute(deleteCommentsRequest, with: managedObjectContext)
        } catch let error as NSError {
            // TODO: handle the error
        }
    }
    
    // Entity for Name
    func entityForName(entityName: String) -> NSEntityDescription {
        return NSEntityDescription.entity(forEntityName: entityName, in: self.managedObjectContext)!
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "TestAppIncode_Group", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
}
