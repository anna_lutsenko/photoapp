//
//  CommentModel.swift
//  TestAppIncode Group
//
//  Created by Anna on 07.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation

protocol CommentModel {
    var id : Int {get}
    var name : String {get}
    var body : String {get}
    var email : String {get}
}
