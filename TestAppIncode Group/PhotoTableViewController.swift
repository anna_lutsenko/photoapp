//
//  PhotoTableViewController.swift
//  TestAppIncode Group
//
//  Created by Anna on 07.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit

class PhotoTableViewController: UITableViewController {
    
    var viewModel: ViewModel = ViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.getData(succeess: { 
            self.tableView.reloadData()
        }) { (error) in
            // TODO: Show Error
        }
    }

}

extension PhotoTableViewController {
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.photosCount
    }
    
    
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
    
        let cell : PhotoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! PhotoTableViewCell
        
        cell.labelTitleImg.text = viewModel.photoTitle(at: index)
        cell.labelUserName.text = viewModel.commentTitle(at: index)
        cell.labelUserEmail.text = viewModel.commentAuthorEmail(at: index)
        cell.labelComment.text = viewModel.comment(at: index)
        cell.photoImage.image = viewModel.defaultImage
        viewModel.photo(at: index) { [weak cell] (image) in
            cell?.photoImage?.image = image
        }
     
        return cell
     }

}
