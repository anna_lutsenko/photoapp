//
//  Photo.swift
//  TestAppIncode Group
//
//  Created by Anna on 07.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit

struct Photo : PhotoModel {
    var id : Int
    var title : String
    var url : String
    
    var comments : [CommentModel] = []
    
    init(id: Int, title: String, url: String) {
        self.id = id
        self.title = title
        self.url = url 
    }
    
    func getImage(succeess: @escaping (UIImage)->()) {
        RequestManager.instance.getImageWithURL(url: url, id: id) { (image) in
            DispatchQueue.main.async {
                succeess(image)
            }
        }
    }
}
