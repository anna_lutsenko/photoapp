//
//  DataProviderProtocol.swift
//  TestAppIncode Group
//
//  Created by Anna on 08.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation

protocol DataProviderProtocol {
    func getPhotos(succeess: @escaping ([PhotoModel])->(), failure: @escaping (Error)->())
}
