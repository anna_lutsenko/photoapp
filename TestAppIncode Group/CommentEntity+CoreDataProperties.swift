//
//  CommentEntity+CoreDataProperties.swift
//  TestAppIncode Group
//
//  Created by Anna on 07.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import CoreData


extension CommentEntity {

//    @nonobjc public class func fetchRequest() -> NSFetchRequest<CommentEntity> {
//        return NSFetchRequest<CommentEntity>(entityName: "CommentEntity")
//    }
    
    

    @NSManaged public var commentEntityBody: String?
    @NSManaged public var commentEntityEmail: String?
    @NSManaged public var commentEntityId: Int16
    @NSManaged public var commentEntityName: String?
    @NSManaged public var photo: PhotoEntity?

}

extension CommentEntity: CommentModel {
    var name: String {
        return commentEntityName ?? ""
    }
    
    var id: Int {
        return Int(self.commentEntityId)
    }
    
    var email: String {
        return self.commentEntityEmail ?? ""
    }
    
    var body: String {
        return self.commentEntityBody ?? ""
    }
}
