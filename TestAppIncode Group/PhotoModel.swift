//
//  PhotoModel.swift
//  TestAppIncode Group
//
//  Created by Anna on 07.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit

protocol PhotoModel {
    var id : Int {get}
    var title : String {get}
    var url : String {get}
    
    var comments : [CommentModel] {get}
    func getImage(succeess: @escaping (UIImage)->())
}
