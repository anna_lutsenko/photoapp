//
//  CommentEntity+CoreDataClass.swift
//  TestAppIncode Group
//
//  Created by Anna on 07.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import CoreData

@objc(CommentEntity)
public class CommentEntity: NSManagedObject {
    convenience init() {
        
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "CommentEntity"), insertInto: CoreDataManager.instance.managedObjectContext)
    }
}
