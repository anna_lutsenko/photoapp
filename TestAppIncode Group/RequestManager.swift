//
//  RequestManager.swift
//  TestAppIncode Group
//
//  Created by Anna on 07.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

struct URLConstants {
    static let photosURL = "https://jsonplaceholder.typicode.com/photos"
    static let commentsURL = "https://jsonplaceholder.typicode.com/comments"
}

class RequestManager : DataProviderProtocol {
    
    static let instance = RequestManager()
    
    func getPhotos(succeess: @escaping ([PhotoModel])->(), failure: @escaping (Error)->()) {
        downloadPhotos(succeess: { (photosResponse) in
            self.downloadComments(succeess: { (comments) in
                var photos = photosResponse
                self.connectCommentsToPhotos(&photos, comments: comments)
                self.savePhotos(photos: photos)
                succeess(photos)
            }, failure: { (error) in
                self.savePhotos(photos: photosResponse)
                succeess(photosResponse)
                failure(error)
            })
        }, failure: failure)
        
    }
    
    func getImageWithURL(url: String, id: Int, succeess: @escaping (UIImage)->()) {
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                succeess(image)
                self.savePhotoToStorage(id: id, image: image)
            }
        }
    }
    
    private func savePhotoToStorage(id: Int, image: UIImage) {
        DispatchQueue.main.async {
            let photo = PhotoEntity.photo(with: id)
            let imageData = UIImagePNGRepresentation(image)
            photo?.photoEntityImage = imageData! as NSData
            do {
                try CoreDataManager.instance.managedObjectContext.save()
            } catch _ {
                
            }
        }
    }
    
    private func savePhotos(photos: [PhotoModel]) {
        DispatchQueue.main.async {
            CoreDataManager.instance.clearAllEnteties()
            CoreDataManager.instance.saveToSorage(photos: photos)
        }
    }
    
    private func downloadPhotos(succeess: @escaping ([Photo])->(), failure: @escaping (Error)->()) {
        Alamofire.request(URLConstants.photosURL)
            
            .responseJSON { response in
                
                switch response.result {
                case .success(let json as Array<Dictionary<String, Any>>):
                    succeess(self.parsePhotoResponse(json))
                    break
                case .failure(let error):
                    failure(error)
                default:
                    succeess([])
                }
                
        }
    }
    
    private func downloadComments(succeess: @escaping ([Comment])->(), failure: @escaping (Error)->()) { Alamofire.request(URLConstants.commentsURL)
        .responseJSON { (response) in
            
            switch response.result {
            case .success(let json as Array<Dictionary<String, Any>>):
                succeess(self.parseCommentResponse(json))
                break
            case .failure(let error):
                failure(error)
            default :
                succeess([])
            }
        }
    }
    
    private func parseCommentResponse(_ json: Array<Dictionary<String, Any>>) -> [Comment] {
        var commentArray : [Comment] = []
        for commentDict in json {
            guard let id = commentDict["id"] as? Int,
                let name = commentDict["name"] as? String,
                let body = commentDict["body"] as? String,
                let email = commentDict["email"] as? String else { continue }
            commentArray.append(Comment(id: id, name: name, body: body, email: email))
        }
        return commentArray
    }
    
    private func parsePhotoResponse(_ json: Array<Dictionary<String, Any>>) -> [Photo] {
        var photoArray : [Photo] = []
        for photoDict in json {
            guard let id = photoDict["id"] as? Int,
                let title = photoDict["title"] as? String,
                let url = photoDict["thumbnailUrl"] as? String else { continue }
            photoArray.append(Photo(id: id, title: title, url: url))
        }
        return photoArray
    }
    
    private func connectCommentsToPhotos(_ photos: inout [Photo], comments: [Comment] ) {
        let minIndex = min(photos.count, comments.count)
        for i in 0..<minIndex {
            photos[i].comments.append(comments[i])
        }
    }

}
