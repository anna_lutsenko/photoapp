//
//  ViewModel.swift
//  TestAppIncode Group
//
//  Created by Anna on 07.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit

class ViewModel {
    private var photos: [PhotoModel] = []
    
    func getData(succeess: @escaping ()->(), failure: @escaping (Error)->()) {
        
        DataProviderFabricMethod.dataProvider.getPhotos(succeess: { (photos) in
            self.photos = photos
            DispatchQueue.main.async {
                succeess()
            }
        }, failure: failure)
        
    }
    
    var photosCount: Int {
        return photos.count
    }
    
    var defaultImage: UIImage {
        return #imageLiteral(resourceName: "defaultImage")
    }
    
    func photoTitle(at index: Int) -> String {
        return photos[index].title
    }
    
    func commentAuthorEmail(at index: Int) -> String {
        return photos[index].comments.first?.email ?? ""
    }
    
    func commentTitle(at index: Int) -> String {
        return photos[index].comments.first?.name ?? ""
    }
    
    func comment(at index: Int) -> String {
        return photos[index].comments.first?.body ?? ""
    }
    
    func photo(at index: Int, succeess:@escaping (UIImage)->()) {
        photos[index].getImage(succeess: succeess)
    }
    
    
}
