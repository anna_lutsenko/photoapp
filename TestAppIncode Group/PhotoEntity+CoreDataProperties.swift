//
//  PhotoEntity+CoreDataProperties.swift
//  TestAppIncode Group
//
//  Created by Anna on 07.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit
import CoreData


extension PhotoEntity {

    @NSManaged public var photoEntityId: Int16
    @NSManaged public var photoEntityImage: NSData?
    @NSManaged public var photoEntityURL: String?
    @NSManaged public var photoEntityTitle: String?
    @NSManaged public var comment: NSSet?

}

// MARK: Generated accessors for comment
extension PhotoEntity {

    @objc(addCommentObject:)
    @NSManaged public func addToComment(_ value: CommentEntity)

    @objc(removeCommentObject:)
    @NSManaged public func removeFromComment(_ value: CommentEntity)

    @objc(addComment:)
    @NSManaged public func addToComment(_ values: NSSet)

    @objc(removeComment:)
    @NSManaged public func removeFromComment(_ values: NSSet)

}

extension PhotoEntity {
    static func photo(with id: Int) -> PhotoEntity? {
        let moc = CoreDataManager.instance.managedObjectContext
        let employeesFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "PhotoEntity")
        
        let predicate = NSPredicate(format: "photoEntityId=%ld", id)
        employeesFetch.predicate = predicate
        
        do {
            let objs = try moc.fetch(employeesFetch) as! [PhotoEntity]
            return objs.first
        } catch {
            return nil 
        }
    }
}

extension PhotoEntity : PhotoModel {
    var id : Int {
        return Int(photoEntityId)
    }
    var title : String {
        return photoEntityTitle ?? ""
    }
    var url : String {
        return photoEntityURL ?? ""
    }
    
    var comments : [CommentModel] {
        let commentsSet = comment as? Set<CommentEntity> ?? []
        return Array(commentsSet)
    }
    
    func getImage(succeess: @escaping (UIImage)->()) {
        guard let data: Data = photoEntityImage as Data? else {
            return
        }
        
        if let image = UIImage(data: data) {
            succeess(image)
        }
    }

}
