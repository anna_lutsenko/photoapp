//
//  PhotoEntity+CoreDataClass.swift
//  TestAppIncode Group
//
//  Created by Anna on 07.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import CoreData

@objc(PhotoEntity)
public class PhotoEntity: NSManagedObject {
    convenience init() {
        
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "PhotoEntity"), insertInto: CoreDataManager.instance.managedObjectContext)
    }
}
