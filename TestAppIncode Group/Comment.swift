//
//  Comment.swift
//  TestAppIncode Group
//
//  Created by Anna on 07.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation

struct Comment : CommentModel {
    var id : Int
    var name : String
    var body : String
    var email : String
}
