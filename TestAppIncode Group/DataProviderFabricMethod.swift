//
//  DataProvider.swift
//  TestAppIncode Group
//
//  Created by Anna on 08.09.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation

import Foundation
import SystemConfiguration


enum DataProviderFabricMethod {
    static var dataProvider: DataProviderProtocol {
        if isInternetAvailable() {
            return RequestManager.instance
        } else {
            return CoreDataManager.instance
        }
    }
    
    static fileprivate func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
}
